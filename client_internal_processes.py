
import dash
from dash import html, dcc
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import psutil
import datetime
from collections import defaultdict, deque

# Init
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

table_style = {
    'maxHeight': '300px',
    'overflowY': 'scroll',
    'width': '100%',
    'minWidth': '100%',
}

header_style = {
    'backgroundColor': '#4CAF50',
    'color': 'white',
    'fontWeight': 'bold',
    'fontSize': '16px',
}

cell_style = {
    'backgroundColor': '#f2f2f2',
    'color': 'black',
    'fontSize': '14px',
    'textAlign': 'left',
    'padding': '8px',
}


cpu_usage_data = defaultdict(lambda: deque(maxlen=10)) 
memory_usage_data = defaultdict(list)
last_update_time = datetime.datetime.now()

# App layout
app.layout = dbc.Container([
    html.H1("System Process Monitor"),
    dbc.Row(dbc.Col(html.Div("Refresh Interval (seconds):"))),
    dbc.Row(dbc.Col(dcc.Input(id='interval-input', value=1, type='number', min=1, debounce=True))),
    dbc.Row(dbc.Col(dcc.Interval(id='interval-component', interval=1*1000))),
    dbc.Row(dbc.Col(dcc.Graph(id='process-table'))),
    dbc.Row(dbc.Col(dcc.Graph(id='cpu-usage-graph'))),
    dbc.Row(dbc.Col(dcc.Graph(id='memory-usage-graph')))
])

@app.callback(
    [Output('interval-component', 'interval'),
     Output('process-table', 'figure'),
     Output('cpu-usage-graph', 'figure'),
     Output('memory-usage-graph', 'figure')],
    [Input('interval-component', 'n_intervals'),
     Input('interval-input', 'value')])
def update_process_info(n, interval):
    global last_update_time

    interval_ms = max(1, interval) * 1000
    current_time = datetime.datetime.now()
    processes = []
    headers = ['PID', 'Name', 'CPU %', 'Memory %']

    for proc in psutil.process_iter(['pid', 'name', 'cpu_percent', 'memory_percent']):
        try:
            info = proc.info
            if info['cpu_percent'] is not None:
                processes.append([info['pid'], info['name'], info['cpu_percent'], info['memory_percent']])
                cpu_usage_data[info['pid']].append((current_time, info['cpu_percent']))  # Append new data point
                memory_usage_data[info['pid']] = info['memory_percent']
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass

    processes.sort(key=lambda x: (x[2] is not None, x[2]), reverse=True)

    table_figure = create_table_figure(processes, headers)
    cpu_usage_figure = create_cpu_usage_figure(cpu_usage_data)
    memory_usage_figure = create_memory_usage_figure(memory_usage_data)

    last_update_time = current_time
    return interval_ms, table_figure, cpu_usage_figure, memory_usage_figure



def create_table_figure(processes, headers):
    return {
        'data': [{'type': 'table',
                  'header': {'values': headers, 'align': 'left', 'fill': {'color': header_style['backgroundColor']}, 'font': {'color': header_style['color'], 'size': header_style['fontSize']}},
                  'cells': {'values': list(zip(*processes)), 'align': 'left', 'fill': {'color': [cell_style['backgroundColor'], 'white']}, 'font': {'color': cell_style['color'], 'size': cell_style['fontSize']}}}],
        'layout': {
            'height': 500,
            'margin': {'t': 10, 'b': 10, 'l': 10, 'r': 10}
        }
    }


def create_cpu_usage_figure(cpu_usage_data):
    traces = []
    for pid, data in cpu_usage_data.items():
        if data:  # Check if data is not empty
            times, usages = zip(*data)
            traces.append({
                'x': times, 
                'y': usages, 
                'type': 'scatter', 
                'mode': 'lines+markers', 
                'name': f'PID {pid}'
            })

    figure = {
        'data': traces, 
        'layout': {
            'title': 'CPU Usage Over Time', 
            'xaxis': {'title': 'Time'}, 
            'yaxis': {'title': 'CPU Usage (%)'}
        }
    }
    return figure


def create_memory_usage_figure(memory_usage_data):
    names = [f'PID {pid}' for pid in memory_usage_data.keys()]
    usages = list(memory_usage_data.values())
    return {'data': [{'x': names, 'y': usages, 'type': 'bar'}], 'layout': {'title': 'Memory Usage', 'xaxis': {'title': 'Process'}, 'yaxis': {'title': 'Memory Usage (%)'}}}

if __name__ == '__main__':
    app.run_server(debug=True, port=8090)
