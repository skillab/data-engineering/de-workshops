import sys
from pathlib import Path
sys.path.append(str(Path(__file__).resolve().parents[1]))

import pytest
from fastapi.testclient import TestClient
from unittest.mock import patch, MagicMock
from datetime import datetime
from main import app, EarthquakeEvent, earthquake_region, record_event


@pytest.fixture(scope="module")
def test_client():
    return TestClient(app)

@pytest.fixture
def mock_mongo_find():
    with patch.object(earthquake_region, 'find', autospec=True) as mock_find:
        yield mock_find

@pytest.fixture
def mock_mongo_insert_one():
    with patch.object(earthquake_region, 'insert_one', autospec=True) as mock_insert:
        yield mock_insert


# Fixture to mock datetime.now()
@pytest.fixture
def mock_datetime_now():
    with patch('main.datetime') as mock_datetime:
        mock_datetime.now.return_value = datetime(2023, 12, 12, 17, 21, 5, 657000)
        yield mock_datetime.now

# Fixture to mock random.uniform
@pytest.fixture
def mock_random_uniform():
    with patch('main.random.uniform') as mock_random:
        # This will always return 3.5 for testing purposes
        mock_random.return_value = 3.5
        yield mock_random

def test_get_recent_earthquakes(test_client, mock_mongo_find):
    mock_earthquakes = [{
        "event_id": "0caeaab1-40a3-42c9-aa30-66d4ba157bc5",
        "magnitude": 2.8822491878933842,
        "recorded_at": datetime(2023, 12, 12, 17, 21, 5, 657000).isoformat(),
        "city": "Bucuresti",
        "country": "Romania",
        "latitude": 44.4468,
        "longitude": 26.1025
    }]
    mock_mongo_find.return_value = mock_earthquakes

    response = test_client.get("/recent-earthquakes")
    assert response.status_code == 200

    actual_response = response.json()

    assert actual_response == mock_earthquakes

def test_record_event(mock_datetime_now, mock_random_uniform):
    expected_event = EarthquakeEvent(
        magnitude=3.5,  # As set by mock_random_uniform
        recorded_at=datetime(2023, 12, 12, 17, 21, 5, 657000),  # As set by mock_datetime_now
        city="Bucuresti",
        country="Romania",
        latitude=44.4268,  
        longitude=26.1025 
    )

    actual_event = record_event()

    # Assert all fields
    assert actual_event.magnitude == expected_event.magnitude
    assert actual_event.recorded_at == expected_event.recorded_at
    assert actual_event.city == expected_event.city
    assert actual_event.country == expected_event.country
