import click
from src.db_operations import get_db_info
from src.staging import stage_db
from src.query_handler import run_query
from src.convert_json import convert_json_to_csv

@click.group()
def cli():
    """MongoDB Query CLI Tool"""
    pass

@click.command(name='db-info')
def db_info():
    """Display MongoDB database information."""
    click.echo(get_db_info())

@click.command(name='stage-db-cmd')
@click.argument('database')
@click.argument('collection')
def stage_db_cmd(database, collection):
    """Stage a database and collection."""
    click.echo("Attempting to stage new database and collections..")
    err = stage_db(database, collection)
    if not err:
        click.echo(f"Successfully staged the database to {database} and collection to: {collection}")


@click.command(name='run-query-cmd')
@click.option('--num_docs', default=25, help='Number of documents to retrieve.')
@click.option('--filter', default="{}", help='Filter for query in JSON format.')
def run_query_cmd(num_docs, filter):
    """Run a query on the staged collection."""
    run_query(num_docs, filter)


@click.command(name='convert-cmd')
@click.option('--do-upload', default=False, help='This option allows for direct upload of the converted file into the Postgres Server.')
@click.argument('input_file_path')
@click.argument('output_file_path')
def convert_cmd(input_file_path, output_file_path, do_upload: bool):
    """Convert a JSON file into a flat csv file - revizier"""
    convert_json_to_csv(input_file_path, output_file_path, do_upload)


cli.add_command(db_info)
cli.add_command(stage_db_cmd)
cli.add_command(run_query_cmd)
cli.add_command(convert_cmd)

if __name__ == "__main__":
    
    cli()
