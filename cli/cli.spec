# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

a = Analysis(
    ['main.py'],  # Add any other entry points if needed
    pathex=[],
    binaries=[('configuration.yml', '.'), ('data', 'data')],
    datas=[('src/__init__.py', 'src'),
           ('src/convert_json.py', 'src'),
           ('src/db_operations.py', 'src'),
           ('src/query_handler.py', 'src'),
           ('src/staging.py', 'src'),
           ('src/table_models.py', 'src'),
           ('src/utils.py', 'src')],
    hiddenimports=['json', 'psycopg2', 'pymongo', 'sqlalchemy', 'yaml', 'pandas'],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
)

pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='cli',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    console=True,
)

coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='cli',
)
