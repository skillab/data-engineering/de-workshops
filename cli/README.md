## Before running, make sure to create a new virtual env and install requirements:

(Linux / Macbook)

`python -m virtualenv pycli`

`source pycli/bin/activate`

`cd cli`

`pip install -r requirements.txt`

(Windows)

`python -m virtualenv pycli`

`.\pycli\Script\activate`

`cd cli`

`pip install -r requirements.txt`


### Displaying MongoDB Database Information
`python main.py db-info`

### Staging a Specific Database and Collection
`python main.py stage-db-cmd <database_name> <collection_name>`

Examples
`python main.py stage-db-cmd earthquakes romania`

### Running Queries on the Staged Collection
`python main.py run-query-cmd`

`python main.py run-query-cmd --num_docs 5`

`python main.py run-query-cmd --filter '{"address.market": "Barcelona"}'`

### Running Convert from json to csv 

`python main.py convert-cmd --do-upload <BOOL> <input_file_path> <output_file_path>`

Example:
`python main.py convert-cmd --do-upload False data/query_results.json data/transactions.csv`

### Compile binary
Make sure the correct Python Environment is set (pycli for example): `source pycli/bin/activate`
`pyinstaller cli.spec -y`

### Running the test suite
Make sure the correct Python Environment is set (pycli for example): `source pycli/bin/activate`
`export PYTHONPATH=$PYTHONPATH:$(pwd)`
`pytest --cov=src tests/`

### Docker Commands
In case you wish to package and distribute the application as a Docker Container, follow the following steps:

* Create an image based off the existing Dockerfile: 
    `docker build -t mongodb-cli-tool .`

* Create a volume (one time), useful for testing the development environment from the host machine: 
    `docker volume create mongodb-cli-data`

* Create a running, interactive container based off the project: 
    `docker run -it --name mongodb-cli-container -v "$(pwd)":/usr/src/app mongodb-cli-tool`


### Running with MageAI
* compile binary using the command: `pyinstaller cli.spec -y`;
* copy-paste the resulting `dist` folder under the mage program directory;
* ensure there is a root folder named `data`;
* activate the Python `pymage` environment;
* On Linux and Macbook, applications might need an express statement to allow programs executed from Shells to use more than the predefined ULimit files. Run: 
`export ULIMIT_NO_FILE=16000`;
* make sure to change directory into the mage program: `cd mage`;
* run: `mage start .`