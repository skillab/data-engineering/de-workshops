"""
This module defines SQL statements for creating database tables.
It provides SQL statements to create tables for transactions, customers, and accounts.
"""

CREATE_TRANSACTIONS_TABLE = """
    CREATE TABLE IF NOT EXISTS transactions (
        account_id VARCHAR(10),
        bucket_start_date DATE,
        bucket_end_date DATE,
        date DATE,
        amount DECIMAL(100, 50),
        transaction_code VARCHAR(24),
        symbol VARCHAR(24),
        price DECIMAL(100, 50),
        total DECIMAL(100, 50)
    );
"""

CREATE_CUSTOMERS_TABLE = """
    CREATE TABLE IF NOT EXISTS customers (
        username VARCHAR(50),
        name VARCHAR(100),
        address VARCHAR(200),
        birthdate DATE,
        email VARCHAR(100),
        active BOOLEAN
    );
"""

CREATE_ACCOUNTS_TABLE = """
    CREATE TABLE IF NOT EXISTS accounts (
        account_id VARCHAR(10),
        "limit" INT
    );
"""