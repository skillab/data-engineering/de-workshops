"""
This module is responsible for handling the staging logic for database operations.
It includes functions for setting up and retrieving staged database and collection information.
"""
import os
import json
from typing import NoReturn, Dict, Union

from .db_operations import CONFIG, get_staged_db_collection, get_mongo_client


def stage_db(database: str, collection: str) -> Union[NoReturn, ConnectionError]:
    """
    Stores database and collection names in a JSON file for staging purposes.

    Args:
    - database (str): The name of the database to be used.
    - collection (str): The name of the collection to be used.

    Returns:
    - NoReturn: This function does not return anything.
    """
    # Get the existing staged database and collection information
    staged_info = get_staged_db_collection()

    # Create a MongoClient instance to validate the database and collection
    mongo_client = get_mongo_client()

    # Check if the provided database exists
    if database not in mongo_client.list_database_names():
        print(f"Error: The database '{database}' does not exist in the MongoDB environment.")
        mongo_client.close()
        return ConnectionError

    # Switch to the provided database to check if the collection exists
    db = mongo_client[database]
    if collection not in db.list_collection_names():
        print(f"Error: The collection '{collection}' does not exist in the '{database}' database.")
        mongo_client.close()
        return ConnectionError

    # If both database and collection exist, update the staged information
    staged_info["database"] = database
    staged_info["collection"] = collection

    # Store the updated information in the JSON file
    with open("staged_database.json", "w") as file:
        json.dump(staged_info, file)

    mongo_client.close()


def get_staged_db_collection() -> Dict[str, str]:
    """
    Reads the staged database and collection names from a JSON file.

    Returns:
    - dict: A dictionary containing the 'database' and 'collection' names. Returns default values if the staging file does not exist.
    """
    if not os.path.exists("staged_database.json"):
        print("Using default database and collection information. Please use the stage-db-cmd command to change the database and collection link.\n")
        return {"database": CONFIG['mongodb']['default_database'], "collection": CONFIG['mongodb']['default_collection']}
    with open("staged_database.json", "r") as file:
        return json.load(file)
