"""
This module provides the logic for processing and executing queries in MongoDB.
It includes functions to run specified queries and handle the resulting data.
"""
import json
from typing import List


from .staging import get_staged_db_collection
from .utils import JSONEncoder, CONFIG
from .db_operations import MONGO_CLIENT


def run_query(num_docs: int, filter_query: str) -> List[dict]:
    """
    Executes a MongoDB query and writes the results to a file.

    Args:
    - num_docs (int): The maximum number of documents to return.
    - filter_query (str): The JSON string representing the query filter.

    Returns:
    - List[dict]: A list of dictionaries representing the query results.
    """
    staging_info = get_staged_db_collection()
    db = MONGO_CLIENT[staging_info["database"]]
    collection = db[staging_info["collection"]]

    filter_dict = json.loads(filter_query)
    results = collection.find(filter_dict).limit(num_docs)

    results_list = [doc for doc in results]

    with open(CONFIG["application"]["query_result"], "w") as file:
        json.dump(results_list, file, cls=JSONEncoder)  # Use the custom encoder here

    MONGO_CLIENT.close()
