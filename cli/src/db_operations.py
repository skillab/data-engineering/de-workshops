"""
This module contains functions and logic for database operations.
It includes connections to different databases (e.g., PostgreSQL, MongoDB)
and related database functionalities.
"""
import os
import json
import psycopg2
from typing import Optional, Dict
from pymongo import MongoClient
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine

from .utils import CONFIG

def get_mongo_client() -> MongoClient:
    """
    Create and return a MongoClient instance for connecting to a MongoDB database.

    Returns:
    - MongoClient: A client instance connected to the specified MongoDB.
    """
    uri = f"{CONFIG['mongodb']['mongo_connection_type']}://{CONFIG['mongodb']['user']}:{CONFIG['mongodb']['password']}@{CONFIG['mongodb']['host']}/{CONFIG['mongodb']['default_database']}?{CONFIG['mongodb']['query_string_options']}"
    return MongoClient(uri)

def get_postgres_client() -> Optional[psycopg2.extensions.connection]:
    """
    Establish and return a connection to a PostgreSQL database.

    Returns:
    - psycopg2.extensions.connection: A connection object to the PostgreSQL database, or None if an error occurs.
    """
    try:
        conn = psycopg2.connect(
            dbname=CONFIG['postgres']['default_database'],
            user=CONFIG['postgres']['user'],
            password=CONFIG['postgres']['password'],
            host=CONFIG['postgres']['host'],
            sslmode="require"
        )

        return conn

        # Postrges server exception handler
    except Exception as e:
        print(f"Postgres Server is unavailable, please try again. \n Full Error: {e}")
        
def get_postgres_engine() -> Optional[Engine]:
    """
    Create and return a SQLAlchemy engine for the PostgreSQL database.

    Returns:
    - Engine: A SQLAlchemy engine connected to the specified PostgreSQL database, or None if an error occurs.
    """
    try:
        return create_engine(f"postgresql://{CONFIG['postgres']['user']}:{CONFIG['postgres']['password']}@{CONFIG['postgres']['host']}/{CONFIG['postgres']['default_database']}?sslmode=require")
    except Exception as e:
        print(f"Postgres Engine could not connect to the Server. \n Full Error: {e}")


def get_staged_db_collection() -> Dict[str, str]:
    """
    Reads database and collection names from a JSON file for staging purposes.

    Returns:
    - dict: A dictionary containing the database and collection names.
    """
    if os.path.exists("staged_database.json"):
        with open("staged_database.json", "r") as file:
            return json.load(file)
    else:
        return {"database": CONFIG['mongodb']['default_database'], "collection": CONFIG['mongodb']['default_collection']}

def get_db_info() -> Dict[str, int]:
    """
    Retrieves and returns the count of documents in each collection of the MongoDB database.

    Returns:
    - dict: A dictionary with collection names as keys and document counts as values.
    """
    staging_info = get_staged_db_collection()

    # Check if the specified database exists in MongoDB
    if staging_info["database"] not in MONGO_CLIENT.list_database_names():
        print(f"Error: The database '{staging_info['database']}' does not exist in the MongoDB environment.")
        return {}

    db = MONGO_CLIENT[staging_info["database"]]
    collections_info = {}
    for collection in db.list_collection_names():
        collections_info[collection] = db[collection].count_documents({})
    MONGO_CLIENT.close()
    return collections_info

MONGO_CLIENT = get_mongo_client()
POSTGRES_CLIENT = get_postgres_client()
POSTGRES_ENGINE = get_postgres_engine()
