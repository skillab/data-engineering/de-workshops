"""
This module is designed for converting JSON data into CSV format and handling the upload of this data to a PostgreSQL database. 
It includes functions for importing JSON files, converting JSON data to a structured CSV format, and optionally uploading the data to a PostgreSQL database using SQLAlchemy. 
The module is also equipped to handle the creation of database tables if they do not already exist.
"""
import json
import pandas as pd
from typing import Optional, Dict, Any

from .utils import get_current_table_type
from .db_operations import POSTGRES_CLIENT, POSTGRES_ENGINE, get_staged_db_collection


def import_json_file(file_path: str) -> Optional[Dict[str, Any]]:
    """
    Import data from a JSON file.

    Args:
    - file_path (str): The path to the JSON file to be imported.

    Returns:
    - dict: The data from the JSON file, or None if an error occurs.
    """
    try:
        with open(file_path, 'r') as json_file:
            data = json.load(json_file)
        return data
    except FileNotFoundError:
        print(f"File not found: {file_path}")
        return None
    except json.JSONDecodeError:
        print(f"Invalid JSON format in file: {file_path}")
        return None


def process_transactions_document(json_data: list) -> list:
    """
    Process a list of JSON documents containing transaction data and flatten it.

    Args:
    json_data (list): A list of JSON documents containing transaction information.

    Returns:
    list: A list of dictionaries where each dictionary represents a flattened transaction with common attributes.
    """

    flat_data = [
        {
            "account_id": doc["account_id"],
            "bucket_start_date": doc["bucket_start_date"],
            "bucket_end_date": doc["bucket_end_date"],
            **transaction  # Unpack each transaction dictionary
        }
        for doc in json_data
        for transaction in doc["transactions"]
    ]

    return flat_data

def process_accounts_document(json_data: list) -> list:
    """
    Process a list of JSON documents containing account data and flatten it.

    Args:
    json_data (list): A list of JSON documents containing account information.

    Returns:
    list: A list of dictionaries where each dictionary represents a flattened account with common attributes.
    """
    
    flat_data = [
        {
            "account_id": doc["account_id"],
            "limit": doc["limit"],
        }
        for doc in json_data
    ]

    return flat_data


def process_customers_document(json_data: list) -> list:
    """
    Process a list of JSON documents containing customer data and flatten it.

    Args:
    json_data (list): A list of JSON documents containing customer information.

    Returns:
    list: A list of dictionaries where each dictionary represents a flattened customer with common attributes.
    """

    flat_data = [
        {
            "username": doc["username"],
            "name": doc["name"],
            "address": doc["address"],
            "birthdate": doc["birthdate"],
            "email": doc["email"],
            "active": doc.get("active", False),
        }
        for doc in json_data
    ]

    return flat_data

def convert_json_to_csv(input_file_path: str, output_file_path: str, do_upload: bool) -> Optional[None]:
    """
    Convert JSON data to a CSV file and optionally upload it to a PostgreSQL database.

    Args:
    - input_file_path (str): The path to the input JSON file.
    - output_file_path (str): The path where the output CSV file will be saved.
    - do_upload (bool): Whether to upload the data to the PostgreSQL database.

    Returns:
    - None
    """
    json_data = import_json_file(input_file_path)

    staged_db_collection = get_staged_db_collection()

    current_collection = staged_db_collection["collection"]
    
    if json_data is not None:

        flat_data = []

        try:
            match current_collection:
                case "transactions":
                    flat_data = process_transactions_document(json_data)
                case "accounts":
                    flat_data = process_accounts_document(json_data)
                case "customers":
                    flat_data = process_customers_document(json_data)            
        except ValueError as e:
            raise (f"Got an unmapped collection type: {current_collection}. {e}.")

        df = pd.DataFrame(flat_data)
        
        df = df.apply(lambda x: x.map(lambda cell: cell.replace('\n', ' ') if isinstance(cell, str) else cell))

        df.to_csv(output_file_path, index=False, quotechar='"')

        if bool(do_upload):
            
            try:

                create_table_sql_check = get_current_table_type(current_collection)

                cursor = POSTGRES_CLIENT.cursor()
                
                # Execute the command
                cursor.execute(create_table_sql_check)

                POSTGRES_CLIENT.commit()

                df.to_sql(current_collection, POSTGRES_ENGINE, if_exists='append', index=False)

            except Exception as e:
                print(f"Error while attempting to execute the Postgres SQL command. \n Full Error: {e}")

                
            finally:
                POSTGRES_CLIENT.close()
