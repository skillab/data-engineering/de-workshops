"""
This module provides utility functions and data types for working with databases and configuration settings.
It includes functions for loading configuration from a YAML file, custom JSON encoding for specific types,
and a utility function to determine the appropriate database table type based on document type strings.
"""

import json
import yaml
from typing import Dict, Any

from enum import Enum, auto
from datetime import datetime
from bson import ObjectId, Decimal128

from .table_models import CREATE_TRANSACTIONS_TABLE, CREATE_CUSTOMERS_TABLE, CREATE_ACCOUNTS_TABLE

class MongoDocumentType(Enum):
    TRANSACTION = "transactions"
    CUSTOMERS = "customers"
    ACCOUNTS = "accounts"

def get_current_table_type(document_type_str: str) -> str:
    """
    Determine the appropriate database table type based on the provided document type string.

    Args:
    document_type_str (str): A string representing the document type.

    Returns:
    str: The corresponding SQL statement to create the database table for the given document type.

    Raises:
    ValueError: If the document type is not recognized.
    """
    
    try:
        document_type = MongoDocumentType(document_type_str)
        
        match document_type.value:

            case MongoDocumentType.TRANSACTION.value:
                return CREATE_TRANSACTIONS_TABLE
            
            case MongoDocumentType.CUSTOMERS.value:
                return CREATE_CUSTOMERS_TABLE
            
            case MongoDocumentType.ACCOUNTS.value:
                return CREATE_ACCOUNTS_TABLE

    except ValueError as e:
        raise (f"Got an unmapped collection type: {document_type}. {e}.")


# Function to load configuration
def load_config() -> Dict[str, Any]:
    """
    Loads configuration settings from a YAML file.

    Returns:
    - Dict[str, Any]: A dictionary containing configuration settings.
    """
    with open("configuration.yml", "r") as file:
        return yaml.safe_load(file)


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, ObjectId):
            return str(obj)
        if isinstance(obj, Decimal128):
            return str(obj)  # or float(str(obj)) for floating point representation
        return json.JSONEncoder.default(self, obj)

CONFIG = load_config()
