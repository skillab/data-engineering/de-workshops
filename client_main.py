from dash import Dash, html, dcc, Input, Output
import plotly.express as px
import json
import plotly.graph_objs as go
import threading
import websocket
import pandas as pd
import numpy as np

# Initialize data storage
data = {
    'event_id': [],
    'magnitude': [],
    'recorded_at': [],
    'city': [],
    'country': [],
    'latitude': [],
    'longitude': []
}

# Websocket client setup
def on_message(ws, message):
    new_data = json.loads(message)
    data['event_id'].append(new_data['event_id'])
    data['magnitude'].append(new_data['magnitude'])
    data['recorded_at'].append(new_data['recorded_at'])
    data['city'].append(new_data['city'])
    data['country'].append(new_data['country'])
    data['latitude'].append(new_data['latitude'])
    data['longitude'].append(new_data['longitude'])

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print("### closed ###")

def on_open(ws):
    def run(*args):
        while True:
            pass
    threading.Thread(target=run).start()

websocket.enableTrace(True)
ws = websocket.WebSocketApp("ws://localhost:8000/ws/earthquakes",
                            on_open=on_open,
                            on_message=on_message,
                            on_error=on_error,
                            on_close=on_close)

wst = threading.Thread(target=ws.run_forever)
wst.daemon = True
wst.start()

# Dash app setup
app = Dash(__name__)
app.layout = html.Div([
    html.H1("Earthquake Data Visualizations"),
    html.Hr(),
    html.Div(className='Live Scatterplot', children=[
        dcc.Graph(id='live-scatterplot')
    ]),
    html.Div(className='Live Density Map', children=[
        dcc.Graph(id='live-density-plot')
    ]),
        html.Div(className='Live heatmap ', children=[
        dcc.Graph(id='live-heatmap-density')
    ]),
    dcc.Interval(
        id='interval-component',
        interval=1*1000,  # in milliseconds
        n_intervals=0
    )
])

@app.callback(Output('live-scatterplot', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_scatterplot(n):

    # Create scatter plot
    scatter = go.Scatter(x=data['recorded_at'], y=data['magnitude'], mode='markers', name='Data Points')

    # Create line connecting the points
    line = go.Scatter(x=data['recorded_at'], y=data['magnitude'], mode='lines', name='Line Connect', line=dict(color='gray'))

    return {
        'data': [scatter, line],
        'layout': go.Layout(
            xaxis={'title': 'Recorded At',
                #    'tickformat': '%H:%M:%S',  # Format to show hours, minutes, and seconds
                #    'dtick': 1000,  # Set tick interval to 1 second (1000 milliseconds)
            },
            yaxis={'title': 'Magnitude'},
            title='Magnitudinea Cutremurelor in timp'
        )
    }

@app.callback(Output('live-density-plot', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_heatmap(n):
    if data['latitude'] and data['longitude']:
        heatmap = go.Figure(data=go.Histogram2dContour(
            x=data['latitude'],
            y=data['longitude'],
            colorscale='Cividis',  # Change this to your preferred colorscale
            contours=dict(
                coloring='heatmap'
            ),
            ncontours=25
        ))
        heatmap.update_layout(
            title='Harta epicentrelor cu cutremure',
            xaxis=dict(title='Latitude'),
            yaxis=dict(title='Longitude')
        )
    else:
        heatmap = go.Figure()

    return heatmap


# Update heatmap callback
@app.callback(Output('live-heatmap-density', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_heatmap(n):
   
    if data['latitude'] and data['longitude']:
        heatmap = go.Figure(data=go.Histogram2d(
            x=data['latitude'],
            y=data['longitude'],
            colorscale='Viridis',
            nbinsx=20,
            nbinsy=20
        ))
        heatmap.update_layout(
            title='Densitatea Cutremurelor',
            xaxis=dict(title='Latitude'),
            yaxis=dict(title='Longitude')
        )
    else:
        heatmap = go.Figure()

    return heatmap


if __name__ == '__main__':
    app.run_server(debug=True)
