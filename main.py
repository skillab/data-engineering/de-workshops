
import time
import json
import uuid
import random
import asyncio
import threading
from dataclasses import field
from pydantic import BaseModel
from pydantic.json import pydantic_encoder
from pymongo import MongoClient
from typing import List, Optional
from datetime import datetime, timedelta
from fastapi import FastAPI, HTTPException, WebSocket



# MongoDB setup
uri = "mongodb+srv://participant:m1A9X4ynyfZ7diCX@skillab-cluster.tbfsbxd.mongodb.net/?retryWrites=true&w=majority"
mongo_client = MongoClient(uri, uuidRepresentation="standard")
db = mongo_client['earthquakes']
earthquake_region = db['romania']

connected_clients = set()


class EarthquakeQuery(BaseModel):
    time_back: Optional[int] = None  # Number of minutes back to query
    city: Optional[str] = None       # Optional city name for filtering
    limit: Optional[int] = None      # Optional limit for number of results


class EarthquakeEvent(BaseModel):
    event_id: uuid.UUID = field(default_factory=lambda: str(uuid.uuid4()))
    magnitude: float
    recorded_at: datetime
    city: str
    country: str
    latitude: float
    longitude: float
    event_id: str
    _id: Optional[str] = None
   
    class Config:
        json_encoders = {
            datetime: lambda v: v.isoformat(),
        }

# FastAPI app setup
app = FastAPI()

# Global variable to control the process
is_process_active = False

# Pydantic model for controlling the machine
class MachineCommand(BaseModel):
    command: str



# Function to generate and insert events
async def generate_and_insert_events():
    while True:
        if is_process_active:
            event = record_event()
            insert_earthquake_event(event)
            await broadcast_event(event)
        await asyncio.sleep(1)


async def broadcast_event(event: EarthquakeEvent):
    event_json = json.dumps(event.model_dump(), default=pydantic_encoder)
    for client in connected_clients:
        try:
            print(f"inside here with event: {event}")
            # await client.send_json(event.model_dump())
            await client.send_text(event_json)
        except Exception as e:
            print(f"I'm the error unfortunately: {e}")
            pass 

# Function to record an event
def record_event() -> EarthquakeEvent:
     return EarthquakeEvent(
        magnitude=random.uniform(2.5, 4.5),
        recorded_at=datetime.now(),
        city="Bucuresti",
        country="Romania",
        latitude=round(44.4268 + 0.01 * (round(random.uniform(1, 5)) % 3) - 0.005 * (round(random.uniform(1, 5)) % 2), 4),
        longitude=round(26.1025 + 0.01 * (round(random.uniform(1, 5)) % 3) - 0.005 * (round(random.uniform(1, 5)) % 2), 4)
    )

# Function to insert an event into MongoDB
def insert_earthquake_event(event: EarthquakeEvent):
    # Convert the Pydantic model instance to a dictionary
    event_dict = event.model_dump()
    
    # Exclude fields not needed for MongoDB storage, like '_id'
    event_dict.pop('_id', None)
    
    earthquake_region.insert_one(event_dict)
    print(f"Inserted event ID: {uuid.uuid4()}")

# FastAPI endpoint to control the machine
@app.post("/earthquake-machine")
async def control_machine(command: MachineCommand):
    global is_process_active
    if command.command.lower() == "on":
        is_process_active = True
        return {"status": "Earthquake machine turned ON"}
    elif command.command.lower() == "off":
        is_process_active = False
        return {"status": "Earthquake machine turned OFF"}
    else:
        raise HTTPException(status_code=400, detail="Invalid command")



# Existing setup code for FastAPI, MongoDB, and the EarthquakeEvent dataclass...

@app.get("/recent-earthquakes", response_model=List[EarthquakeEvent])
async def get_recent_earthquakes():
    ten_minutes_ago = datetime.now() - timedelta(minutes=10)
    recent_earthquakes = list(earthquake_region.find({"recorded_at": {"$gte": ten_minutes_ago}}))

    return [EarthquakeEvent(**quake) for quake in recent_earthquakes]


@app.post("/recent-earthquakes", response_model=List[EarthquakeEvent])
async def get_earthquakes(query: EarthquakeQuery):
    query_time = datetime.now() - timedelta(minutes=query.time_back) if query.time_back else datetime.fromtimestamp(0)
    
    mongo_query = {"recorded_at": {"$gte": query_time}}
    if query.city:
        mongo_query["city"] = query.city

    # Apply the limit if provided, else fetch all matching records
    result_limit = query.limit if query.limit is not None else 0  # 0 in MongoDB's find() means no limit

    earthquakes = list(earthquake_region.find(mongo_query).limit(result_limit))

    return [EarthquakeEvent(**quake) for quake in earthquakes]

@app.get("/", response_model=dict)
async def home():
    return {"message": "Bun venit la Skillab!"}


@app.websocket("/ws/earthquakes")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    connected_clients.add(websocket)
    try:
        while True:
            await websocket.receive_text()  # Keep the connection open
    except Exception as e:
        pass
    finally:
        connected_clients.remove(websocket)


# Start the background process
# background_thread = threading.Thread(target=generate_and_insert_events, daemon=True)
# background_thread.start()

background_thread = threading.Thread(target=lambda: asyncio.run(generate_and_insert_events()), daemon=True)
background_thread.start()


