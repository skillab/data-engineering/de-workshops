# data_engineering_tests.py

import pytest

# Function 1: Data Transformation Function
def transform_data(data, transformation_func):
    return [transformation_func(x) for x in data]

# Function 2: Data Validation Class
class DataValidator:
    def validate(self, data):
        # Example validation: Check if all elements are integers
        return all(isinstance(x, int) for x in data)

# Function 3: ETL Pipeline Function
def etl_pipeline(source, destination, transform_func):
    transformed_data = [transform_func(x) for x in source]
    destination.extend(transformed_data)

# Function 4: Data Quality Monitoring Class
class DataQualityMonitor:
    def check_duplicates(self, data):
        # Example check: Check for duplicate elements
        return len(data) != len(set(data))

# Function 5: Data Serialization and Deserialization Functions
def serialize_data(data, format):
    if format == 'json':
        import json
        return json.dumps(data)
    else:
        raise ValueError("Unsupported format")

def deserialize_data(data, format):
    if format == 'json':
        import json
        return json.loads(data)
    else:
        raise ValueError("Unsupported format")

# PyTest Test Examples
def test_transform_data():
    data = [1, 2, 3, 4, 5]
    transformed_data = transform_data(data, lambda x: x * 2)
    assert transformed_data == [2, 4, 6, 8, 10]

def test_data_validator():
    validator = DataValidator()
    data = [1, 2, 'three', 4]
    assert validator.validate(data) == False

def test_etl_pipeline():
    source_data = [1, 2, 3]
    destination = []
    transform_func = lambda x: x * 2
    etl_pipeline(source_data, destination, transform_func)
    assert destination == [2, 4, 6]

def test_data_quality_monitor():
    monitor = DataQualityMonitor()
    data = [1, 2, 3, 2, 4, 5]
    assert monitor.check_duplicates(data) == True

def test_serialize_deserialize_data():
    data = {'name': 'Vlad', 'age': 32}
    serialized_data = serialize_data(data, 'json')
    deserialized_data = deserialize_data(serialized_data, 'json')
    assert deserialized_data == data

# Run the tests
if __name__ == "__main__":
    pytest.main(['-v', 'data_engineering_tests.py'])
