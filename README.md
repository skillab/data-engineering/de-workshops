
# Data Engineering Workshop (de-workshop)

This repository contains Python Jupyter Notebooks focused on various aspects of Data Engineering. Throughout the workshop, we explore different facets of data processing, manipulation, and visualization using Python.

## Table of Contents

1. [Getting Started](#getting-started)
2. [Notebooks](#notebooks)
3. [Datasets](#datasets)

## Getting Started

To get started, clone this repository and set up a virtual environment:

```bash
git clone https://gitlab.com/skillab/data-engineering/de-workshops
cd de-workshop
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Then, launch Jupyter Notebook:

```bash
jupyter notebook
```

## Notebooks

* Curs II. https://app.noteable.io/published/91295026-2016-4567-9cde-fad603a43ba3
* Curs III. https://app.noteable.io/published/2d303409-d57f-4772-acd0-c903cc4888d9
* Curs IV. https://app.noteable.io/published/7f3f4e86-ac98-440c-8191-27383d364679
* Curs V. https://app.noteable.io/published/525751c6-e5e0-43d5-89b4-27ed5e65db99
* Curs VI. https://app.noteable.io/published/8848041e-06e2-424a-ba08-6ac6125c15a2
* Curs VII. https://colab.research.google.com/drive/1BQ3sKv33oRhhObXbxU__lJ36LF2aEU8q?usp=sharing#scrollTo=ykdy5Rxf33Hp

## Datasets

- **Iris Dataset**: A classic dataset in machine learning and statistics. It contains measurements for 150 iris flowers from three different species.
- **Online Retail Dataset**: E-commerce transactions data from a UK-based online retail.

